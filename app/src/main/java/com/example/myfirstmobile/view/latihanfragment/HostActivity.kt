package com.example.myfirstmobile.view.latihanfragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myfirstmobile.R
import com.example.myfirstmobile.databinding.ActivityHostBinding
import com.example.myfirstmobile.view.latihanfragment.home.HomeFragment
import com.example.myfirstmobile.view.latihanfragment.inputbiodata.InputBiodataFragment

class HostActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHostBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHostBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initBiodataFragment()

    }

    private fun initBiodataFragment() {
        val homeFragment = HomeFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, homeFragment)
            .commit()
    }
}