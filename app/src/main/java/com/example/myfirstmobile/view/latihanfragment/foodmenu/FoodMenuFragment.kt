package com.example.myfirstmobile.view.latihanfragment.foodmenu

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myfirstmobile.databinding.FragmentFoodMenuBinding
import com.example.myfirstmobile.model.CategoryFoodModel
import com.example.myfirstmobile.model.FoodModel
import com.example.myfirstmobile.view.home.CategoryFoodAdapter

class FoodMenuFragment : Fragment() {

    private var binding: FragmentFoodMenuBinding? = null
    private var foodAdapter = FoodAdapter()
    private var categoryFoodAdapter = CategoryFoodAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFoodMenuBinding.inflate(layoutInflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        foodAdapter.setData(populateData())
        categoryFoodAdapter.setData(populateDataCategory())

        binding?.rvFood?.adapter = foodAdapter
        binding?.rvCategoryFood?.adapter = categoryFoodAdapter
        categoryFoodAdapter.addOnClickFoodCategoryItem { categoryFoodModel ->
            val categoryFoodData = populateDataCategory()

            val category = categoryFoodData.map {
                val categoryId = categoryFoodModel.id
                val isSelected = it.id == categoryId
                it.copy(isSelected = isSelected)
            }
            categoryFoodAdapter.setData(category.toMutableList())

            val data = populateData()
            val filterData = data.filter { dataFood ->
                dataFood.category == categoryFoodModel.title
            }
            foodAdapter.setData(filterData.toMutableList())
        }

        binding?.etSearch?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                var data = populateData()
                val filterData = populateData().filter { dataFood ->
                    dataFood.title.contains(s.toString(), ignoreCase = true)

                }
                foodAdapter.setData(filterData.toMutableList())


                val editTextValue = binding?.etSearch?.text
                if (editTextValue?.isEmpty() == true) {
                    binding?.ivClose?.visibility = View.INVISIBLE
                } else {
                    binding?.ivClose?.visibility = View.VISIBLE
                }
            }
        })
        binding?.ivClose?.setOnClickListener {
            binding?.etSearch?.setText("")
        }
    }
    private fun populateData(): MutableList<FoodModel> {
        val listData = mutableListOf(
            FoodModel(
                image = "https://images.tokopedia.net/img/KRMmCm/2022/9/23/2067c6c6-82aa-46a1-8488-97a54c6b6ea4.jpg",
                title = "Coto Makassar",
                subTitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                category = "Local Food"
            ),
            FoodModel(
                image = "https://static01.nyt.com/images/2021/02/14/dining/carbonara-horizontal/carbonara-horizontal-threeByTwoMediumAt2X-v2.jpg",
                title = "Carbonara Spaghetti",
                subTitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                category = "Italian Food"
            ),
            FoodModel(
                image = "https://asset.kompas.com/crops/ftn9FZVAVPPEam89bc-evKfYczo=/0x0:1000x667/750x500/data/photo/2021/10/23/61740c205fcc2.jpg",
                title = "Smoked Salmon Sushi",
                subTitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                category = "Asian Food"
            ),
            FoodModel(
                image = "https://kbu-cdn.com/dk/wp-content/uploads/beef-burger-special.jpg",
                title = "Cheese Burger",
                subTitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                category = "Western Food"
            ),
            FoodModel(
                image = "https://cdn-2.tstatic.net/manado/foto/bank/images/nasgooor.jpg",
                title = "Nasi Goreng Kampung",
                subTitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                category = "Local Food"
            ),
            FoodModel(
                image = "https://asset.kompas.com/crops/TRfLGkRwwRE0OdA4zvmtVujBXSI=/0x0:1000x667/780x390/data/photo/2020/06/02/5ed5f39c7d8fe.jpg",
                title = "Bakso",
                subTitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                category = "Local Food"
            ),
            FoodModel(
                image = "https://img-global.cpcdn.com/recipes/6acdb3e929dadb70/1200x630cq70/photo.jpg",
                title = "Tteokbokki",
                subTitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                category = "Asian Food"
            ),
            FoodModel(
                image = "https://www.foodandwine.com/thmb/Wd4lBRZz3X_8qBr69UOu2m7I2iw=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/classic-cheese-pizza-FT-RECIPE0422-31a2c938fc2546c9a07b7011658cfd05.jpg",
                title = "Classic Cheese Pizza",
                subTitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                category = "Italian Food"
            ),
            FoodModel(
                image = "https://ik.imagekit.io/waters2021/sehataqua/uploads/20220927031341_original.jpg?tr=w-660,h-371,q-50",
                title = "Beef Steak",
                subTitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                category = "Western Food"
            ),
        )
        return listData
    }

    private fun populateDataCategory(): MutableList<CategoryFoodModel> {
        val listData = mutableListOf(
            CategoryFoodModel(id = 1, title = "Local Food", isSelected = false),
            CategoryFoodModel(id = 2, title = "Western Food", isSelected = false),
            CategoryFoodModel(id = 3, title = "Asian Food", isSelected = false),
            CategoryFoodModel(id = 4, title = "Italian Food", isSelected = false)
        )

        return listData
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}