package com.example.myfirstmobile.view.detail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myfirstmobile.databinding.ActivityDetailNewsBinding
import com.example.myfirstmobile.model.NewsModel

class DetailNewsActivtiy : AppCompatActivity() {

    private lateinit var binding: ActivityDetailNewsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailNewsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setDataToViewDetail()

    }

    private fun setDataToViewDetail() {
        val data = intent.getParcelableExtra<NewsModel>(DATA_NEWS)
        binding.ivDetailNews.setImageResource(data?.image ?: 0)
        binding.tvTitleNews.text = data?.title
        binding.tvSubtitleNews.text = data?.subTitle

        binding.componentAppBar.tvAppbar.text = data?.title
        binding.componentAppBar.ivBack.setOnClickListener{
            this.onBackPressed()
        }

    }

    companion object {
        private const val DATA_NEWS = "dataNews"
        fun navigateToActivityDetail(
            activity: Activity, dataNews: NewsModel
        ) {
            val intent = Intent(activity, DetailNewsActivtiy::class.java)
            intent.putExtra(DATA_NEWS, dataNews)
            activity.startActivity(intent)
        }
    }
}