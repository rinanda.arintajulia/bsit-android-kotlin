package com.example.myfirstmobile.view.latihanfragment.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.myfirstmobile.R
import com.example.myfirstmobile.databinding.HomeFragmentBinding
import com.example.myfirstmobile.view.latihanfragment.foodmenu.FoodMenuFragment
import com.example.myfirstmobile.view.latihanfragment.inputbiodata.InputBiodataFragment

class HomeFragment : Fragment() {

    private lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnRegister.setOnClickListener {
            navigateToRegister()
        }
        setAppBar()
        binding.btnFood.setOnClickListener {
            navigateToFood()
        }
    }

    private fun setAppBar() {
        // untuk layout yg pertama tdk butuh tombol back
        binding.componentAppBar.ivBack.isVisible = false
    }

    private fun navigateToRegister() {
        val registerFragment = InputBiodataFragment()
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, registerFragment)
            .addToBackStack(REGISTER_FRAGMENT_KEY)
            .commit()
    }

    private fun navigateToFood() {
        val foodFragment = FoodMenuFragment()
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, foodFragment)
            .addToBackStack(FOOD_FRAGMENT_KEY)
            .commit()
    }

    companion object {
        const val REGISTER_FRAGMENT_KEY = "register"
        const val FOOD_FRAGMENT_KEY = "food menu key"
    }

}
