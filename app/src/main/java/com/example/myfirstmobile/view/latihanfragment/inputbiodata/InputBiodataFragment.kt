package com.example.myfirstmobile.view.latihanfragment.inputbiodata

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.myfirstmobile.R
import com.example.myfirstmobile.databinding.InputBiodataFragmentBinding

class InputBiodataFragment : Fragment() {

    private lateinit var binding: InputBiodataFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = InputBiodataFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnSubmit.setOnClickListener {
            handleSubmitButton()
        }
        setAppBar()

    }

    private fun setAppBar() {
        binding.layoutComponentAppBar.tvAppbar.text = "Register Profile"
        binding.layoutComponentAppBar.ivBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

    }

    private fun handleSubmitButton() {
        val checkEtNama = binding.etNama.text.isNullOrEmpty().not()
        val checkEtAlamat = binding.etAlamat.text.isNullOrEmpty().not()
        val checkEtHp = binding.etHp.text.isNullOrEmpty().not()
        val checkEtEmail = binding.etEmail.text.isNullOrEmpty().not()
        if (checkEtNama && checkEtAlamat && checkEtHp && checkEtAlamat && checkEtEmail) {

            var bundle = Bundle().apply {
                putString(NAME_KEY, binding.etNama.text.toString())
                putString(ALAMAT_KEY, binding.etAlamat.text.toString())
                putString(HP_KEY, binding.etHp.text.toString())
                putString(EMAIL_KEY, binding.etEmail.text.toString())
            }

            val successFragment = SuccessBiodataFragment().apply {
                arguments = bundle
            }
            parentFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, successFragment)
                .commit()

        } else {
            Toast.makeText(context, "Tolong lengkapi biodata", Toast.LENGTH_LONG).show()
        }
    }

    companion object {
        const val NAME_KEY = "name"
        const val ALAMAT_KEY = "alamat"
        const val HP_KEY = "hp"
        const val EMAIL_KEY = "email"
    }
}