package com.example.myfirstmobile.view.latihanfragment.foodmenu

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myfirstmobile.databinding.ItemFoodBinding
import com.example.myfirstmobile.model.FoodModel

class FoodAdapter :
    RecyclerView.Adapter<FoodAdapter.FoodViewHolder>() {

    private var dataFood: MutableList<FoodModel> = mutableListOf()

    fun setData(newData: MutableList<FoodModel>) {
        dataFood = newData
        notifyDataSetChanged()
    }

    inner class FoodViewHolder(val binding: ItemFoodBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindView(data: FoodModel) {
            Glide.with(binding.root.context)
                .load(data.image)
                .into(binding.ivItemFood)


//            binding.ivItemFood.setImageResource(data.image ?: 0)
            binding.tvTitleFood.text = data.title
            binding.tvSubtitleFood.text = data.subTitle

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder =
        FoodViewHolder(
            ItemFoodBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
        holder.bindView(dataFood[position])
    }

    override fun getItemCount(): Int = dataFood.size

}