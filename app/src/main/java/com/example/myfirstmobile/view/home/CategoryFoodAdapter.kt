package com.example.myfirstmobile.view.home

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.myfirstmobile.R
import com.example.myfirstmobile.databinding.ItemCategoryBinding
import com.example.myfirstmobile.databinding.ItemFoodCategoryBinding
import com.example.myfirstmobile.model.CategoryFoodModel
import com.example.myfirstmobile.model.CategoryModel

class CategoryFoodAdapter() : RecyclerView.Adapter<CategoryFoodAdapter.CategoryFoodViewModel>() {

    private var data: MutableList<CategoryFoodModel> = mutableListOf()
    private var onClickList: (CategoryFoodModel) -> Unit = {}

    fun setData(newData: MutableList<CategoryFoodModel>) {
        data = newData
        notifyDataSetChanged()
    }

    fun addOnClickFoodCategoryItem(clickFood: (CategoryFoodModel) -> Unit) {
        onClickList = clickFood
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryFoodViewModel {
        return CategoryFoodViewModel(
            ItemFoodCategoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CategoryFoodViewModel, position: Int) {
        holder.bindingData(data[position])
    }

    override fun getItemCount(): Int = data.size


    inner class CategoryFoodViewModel(val binding: ItemFoodCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindingData(data: CategoryFoodModel) {
            binding.tvFood.text = data.title
            binding.constraintFood.setOnClickListener {
                onClickList(data)
            }
            val (selectedBackgroundRes, selectedColor) = if (data.isSelected ?: false) {
                Pair(R.drawable.background_rounded_selected, Color.WHITE)
            } else {
                Pair(R.drawable.background_rounded_outline_black, Color.BLACK)
            }

            val selectedBackground =
                ContextCompat.getDrawable(binding.root.context, selectedBackgroundRes)

            binding.constraintFood.background = selectedBackground
            binding.tvFood.setTextColor(ColorStateList.valueOf(selectedColor))

        }
    }

}





