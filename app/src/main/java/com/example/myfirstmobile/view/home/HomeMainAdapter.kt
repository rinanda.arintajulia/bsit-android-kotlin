package com.example.myfirstmobile.view.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myfirstmobile.databinding.ItemNewsBinding
import com.example.myfirstmobile.model.NewsModel

class HomeMainAdapter :
    RecyclerView.Adapter<HomeMainAdapter.HomeMainViewholder>() {

    private val dataNews: MutableList<NewsModel> =  mutableListOf()
    private var onClickNews: (NewsModel) -> Unit = {}

    fun addDataNews(newData: List<NewsModel>) {
        dataNews.addAll(newData)
        notifyDataSetChanged()
    }

    fun addOnClickNews(clickNews: (NewsModel) -> Unit){
        onClickNews = clickNews
    }

    inner class HomeMainViewholder(val binding: ItemNewsBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindView(data: NewsModel, onClickNews: (NewsModel) -> Unit) {
            binding.ivItemNews.setImageResource(data.image ?: 0)
            binding.tvTitleNews.text = data.title
            binding.tvSubtitleNews.text = data.subTitle

            binding.llNews.setOnClickListener{
                onClickNews(data)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeMainViewholder
    = HomeMainViewholder(
        ItemNewsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

    )

    override fun onBindViewHolder(holder: HomeMainViewholder, position: Int) {
        holder.bindView(dataNews[position], onClickNews)
    }

    override fun getItemCount(): Int = dataNews.size

}