package com.example.myfirstmobile.view.profile

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myfirstmobile.R
import com.example.myfirstmobile.base.BaseDialog
import com.example.myfirstmobile.databinding.ActivityProfileBinding
import com.example.myfirstmobile.view.login.LoginActivity.Companion.KEY_ADDRESS
import com.example.myfirstmobile.view.login.LoginActivity.Companion.KEY_INPUT
import com.example.myfirstmobile.view.login.LoginActivity.Companion.KEY_NAME

class ProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val name = intent.getStringExtra(KEY_NAME)
        val address = intent.getStringExtra(KEY_ADDRESS)
        val valueInput = intent.getStringExtra(KEY_INPUT)

        binding.tvName.text = name
        binding.tvAddress.text = address

        binding.componentAppBar.tvAppbar.text = "Profile"
        binding.componentAppBar.ivBack.setOnClickListener {
            this.onBackPressed()

        }

        logout()
    }

    private fun logout() {
        binding.btnLogout.setOnClickListener {
            val dialog = BaseDialog(this,
                "Peringatan",
                "Ketika button proceed diklik maka logout", onOkClicked = {
//                    Toast.makeText(this, "Ini button proceed", Toast.LENGTH_LONG).show()
                    finish()
                },
                withImage = true,
//                image = R.drawable.ic_launcher_background
            )
            dialog.setCancelable(false)
            dialog.show()
        }
    }
}