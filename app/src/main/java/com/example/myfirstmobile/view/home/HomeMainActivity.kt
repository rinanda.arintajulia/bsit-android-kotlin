package com.example.myfirstmobile.view.home

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myfirstmobile.R
import com.example.myfirstmobile.databinding.ActivityHomeMainBinding
import com.example.myfirstmobile.model.CategoryModel
import com.example.myfirstmobile.model.NewsModel
import com.example.myfirstmobile.view.detail.DetailNewsActivtiy

class HomeMainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeMainBinding
    private val mainAdapter = HomeMainAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.componentAppBar.tvAppbar.text = "Recycler view activity"
//        binding.componentAppBar.ivBack.visibility = View.GONE
        binding.componentAppBar.ivBack.setOnClickListener{
            this.onBackPressed()
        }

//        val mainAdapter = HomeMainAdapter(
//            dataNews = populateData(),
//            onClickNews = {
//                DetailNewsActivtiy.navigateToActivityDetail(this, it)
//
//            })

        binding.rvNews.adapter = mainAdapter

        val categoryAdapter = CategoryAdapter(data = populateDataForCategory())
        binding.rvListHorizontal.adapter = categoryAdapter

        val data = populateData()
        mainAdapter.addDataNews(data)

        mainAdapter.addOnClickNews { dataNews ->
            DetailNewsActivtiy.navigateToActivityDetail(
                this, dataNews
            )
        }

//        binding.btnAdd.setOnClickListener {
//            val newData = populateData2()
//            mainAdapter.addDataNews(newData)
//        }
    }

    private fun populateData2(): List<NewsModel> {
        return listOf(
            NewsModel(
                image = R.drawable.jenomilan,
                title = "Visual Mewah Jeno NCT Menuju Milan Fashion Week 2023 Gemparkan Media Korea",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),

            NewsModel(
                image = R.drawable.taylorgrammy2,
                title = "Taylor Swift bereaksi atas kemenangan Grammy 2023",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),

            NewsModel(
                image = R.drawable.nyanyan,
                title = "Sambut Nyan Nyan Nyan Day alias Hari Kucing, Intip 5 Anime Jepang yang Punya Karakter Kucing",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            )
        )
    }


    private fun populateData(): List<NewsModel> {
        val listData = listOf<NewsModel>(
            NewsModel(
                image = R.drawable.jenomilan,
                title = "Visual Mewah Jeno NCT Menuju Milan Fashion Week 2023 Gemparkan Media Korea",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),

            NewsModel(
                image = R.drawable.taylorgrammy2,
                title = "Taylor Swift bereaksi atas kemenangan Grammy 2023",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),

            NewsModel(
                image = R.drawable.nyanyan,
                title = "Sambut Nyan Nyan Nyan Day alias Hari Kucing, Intip 5 Anime Jepang yang Punya Karakter Kucing",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            )
        )
        return listData

    }

    private fun populateDataForCategory(): List<CategoryModel> {
        val listData = listOf(
            CategoryModel(
                title = "News",
            ),
            CategoryModel(
                title = "Sport",
            ),
            CategoryModel(
                title = "Education",
            ),
            CategoryModel(
                title = "Fashion",
            ),
            CategoryModel(
                title = "Healthy",
            ),
            CategoryModel(
                title = "Teens",
            ),
            CategoryModel(
                title = "Entertainment",
            ),
            CategoryModel(
                title = "Business",
            ),
            CategoryModel(
                title = "Economic",
            ),
            CategoryModel(
                title = "Social",
            ),
        )
        return listData
    }
}
