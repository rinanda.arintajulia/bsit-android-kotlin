package com.example.myfirstmobile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myfirstmobile.databinding.ActivityPertemuanBinding
import com.example.myfirstmobile.view.biodata.BiodataActivity
import com.example.myfirstmobile.view.login.LoginActivity


class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPertemuanBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPertemuanBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //        Intent >> melakukan peprindahan antara activity
        binding.button1.setOnClickListener {
            val intent = Intent(applicationContext, BiodataActivity::class.java)
            startActivity(intent)
        }

        binding.button2.setOnClickListener {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
        }
    }
}