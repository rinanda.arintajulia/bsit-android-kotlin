package com.example.myfirstmobile.view.biodata

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myfirstmobile.databinding.ActivityBiodataBinding

class BiodataActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBiodataBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBiodataBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        var name = "Rinanda Arinta Julia"
//        Toast.makeText(applicationContext, name, Toast.LENGTH_LONG).show()

//        val textView : TextView = null
//        textView?.findViewById<TextView>(R.id.name)

//        val names = "Rinanda Arinta Julia"
// //       neko_nyan.text = names
//        val ages = 22
//        binding.nekoNyan.text = names
//        binding.age.text = ages.toString()
    }
}