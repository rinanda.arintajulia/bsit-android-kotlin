package com.example.myfirstmobile.base

import android.app.Application
import android.content.Context
import android.content.SharedPreferences

interface SharedPreferenceModule {
    fun provideSharedPreferences(context: Context): SharedPreferences
}