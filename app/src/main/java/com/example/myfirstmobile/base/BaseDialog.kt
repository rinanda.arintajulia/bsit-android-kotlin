package com.example.myfirstmobile.base

import android.app.Dialog
import android.content.Context
import android.media.Image
import android.os.Bundle
import androidx.core.view.isVisible
import com.example.myfirstmobile.R
import com.example.myfirstmobile.databinding.BaseDialogBinding

class BaseDialog(
    context: Context,
    var title: String,
    var subtitle: String,
    private val onOkClicked: (() -> Unit),
    private val withImage: Boolean,
    private val image: Int? = null
) : Dialog(context) {

    private lateinit var binding: BaseDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = BaseDialogBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivDialog.setImageResource(image ?: R.drawable.taylor)

        binding.ivDialog.isVisible = withImage

        binding.tvDialogTitle.text = title
        binding.tvDialogSubtitle.text = subtitle

        binding.btnCancel.setOnClickListener {
            dismissDialog()

        }
        binding.btnProceed.setOnClickListener { onOkClicked.invoke() }

    }

    private fun dismissDialog() {
        dismiss()
    }
}