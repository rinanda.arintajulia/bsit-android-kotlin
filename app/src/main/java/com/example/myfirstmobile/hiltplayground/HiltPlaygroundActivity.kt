package com.example.myfirstmobile.hiltplayground

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myfirstmobile.R
import com.example.myfirstmobile.base.PreferencesHelper
import com.example.myfirstmobile.databinding.ActivityHiltPlaygroundBinding
import com.example.myfirstmobile.model.Engine
import com.example.myfirstmobile.model.FoodModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HiltPlaygroundActivity : AppCompatActivity() {

//    private var _binding : ActivityHiltPlaygroundBinding? = null
//    private val binding get() = _binding!!
    private lateinit var binding: ActivityHiltPlaygroundBinding

    @Inject
    lateinit var preferencesHelper: PreferencesHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHiltPlaygroundBinding.inflate(layoutInflater)
        setContentView(binding.root)

        updateValueLayout()

        binding.btnEditProfile.setOnClickListener {
            val intent = Intent(this, ProfileOverviewActivity::class.java)
            startActivity(intent)
        }

        binding.btnClearPref.setOnClickListener {
            preferencesHelper.clearDataPref()
            updateValueLayout()
        }
//        var engine = Engine()
//        engine.startEngine()
    }

    private fun updateValueLayout() {
        val getNameFromLocal = preferencesHelper.getName()
        val getAddressFromLocal = preferencesHelper.getAddress()

        binding.tvName.text = getNameFromLocal
        binding.tvAddress.text = getAddressFromLocal
    }
}