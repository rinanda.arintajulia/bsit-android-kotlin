package com.example.myfirstmobile.hiltplayground

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myfirstmobile.base.PreferencesHelper
import com.example.myfirstmobile.databinding.ActivityInputProfileBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ProfileOverviewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityInputProfileBinding

    @Inject
    lateinit var preferencesHelper: PreferencesHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInputProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSave.setOnClickListener {
            val nameValue = binding.etInputNama.text
            val alamatValue = binding.etInputAlamat.text

            if (nameValue.isNullOrEmpty().not() && alamatValue.isNullOrEmpty().not()) {
                preferencesHelper.saveName(nameValue.toString())
                preferencesHelper.saveAddress(alamatValue.toString())
                navigateBack()
            }
            else {
                Toast.makeText(this, "ISI DATA DULU LAH !!!", Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun navigateBack() {
        startActivity(Intent(this, HiltPlaygroundActivity::class.java))
    }
}