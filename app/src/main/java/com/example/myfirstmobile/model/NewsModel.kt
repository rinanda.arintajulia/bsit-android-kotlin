package com.example.myfirstmobile.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize // untuk bawa data
data class NewsModel(
    val image: Int?,
    val title: String?,
    val subTitle: String?
) : Parcelable
