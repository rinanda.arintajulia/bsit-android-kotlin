package com.example.myfirstmobile.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoryModel(
    val title: String?
) : Parcelable
