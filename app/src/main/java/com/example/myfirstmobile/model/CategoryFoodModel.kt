package com.example.myfirstmobile.model

data class CategoryFoodModel(
    val id: Int?,
    val title: String?,
    val isSelected: Boolean?
)
