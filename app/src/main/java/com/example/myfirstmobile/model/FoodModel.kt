package com.example.myfirstmobile.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize // untuk bawa data
data class FoodModel(
    val image: String?,
    val title: String,
    val subTitle: String?,
    val category: String
) : Parcelable
